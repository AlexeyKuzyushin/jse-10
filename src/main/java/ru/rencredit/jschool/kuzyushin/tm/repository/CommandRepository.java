package ru.rencredit.jschool.kuzyushin.tm.repository;

import ru.rencredit.jschool.kuzyushin.tm.api.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.constant.ArgumentConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.CommandConst;
import ru.rencredit.jschool.kuzyushin.tm.constant.DescriptionConst;
import ru.rencredit.jschool.kuzyushin.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            CommandConst.HELP, ArgumentConst.HELP, DescriptionConst.HELP
    );

    public static final TerminalCommand ARGUMENTS = new TerminalCommand(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, DescriptionConst.ARGUMENTS
    );

    public static final TerminalCommand COMMANDS = new TerminalCommand(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS, DescriptionConst.COMMANDS
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            CommandConst.ABOUT, ArgumentConst.ABOUT, DescriptionConst.ABOUT
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            CommandConst.VERSION, ArgumentConst.VERSION, DescriptionConst.VERSION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            CommandConst.INFO, ArgumentConst.INFO, DescriptionConst.INFO
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            CommandConst.EXIT, null, DescriptionConst.EXIT
    );

    private final String[] ARRAY_ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    private final String[] ARRAY_COMMANDS = getCommands(TERMINAL_COMMANDS);

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    public String[] getArguments(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArgument();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @Override
    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getCommands() {
        return ARRAY_COMMANDS;
    }

    @Override
    public String[] getArguments() {
        return ARRAY_ARGUMENTS;
    }

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getCommand();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }
}
