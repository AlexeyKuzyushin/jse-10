package ru.rencredit.jschool.kuzyushin.tm.api;

public interface ICommandController {

    void exit();

    void showVersion();

    void showAbout();

    void showHelp();

    void showInfo();

    void showCommands();

    void showArguments();
}