package ru.rencredit.jschool.kuzyushin.tm.api;

import ru.rencredit.jschool.kuzyushin.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getArguments();

    String[] getCommands();
}
