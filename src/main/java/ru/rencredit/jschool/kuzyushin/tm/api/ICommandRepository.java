package ru.rencredit.jschool.kuzyushin.tm.api;

import ru.rencredit.jschool.kuzyushin.tm.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArguments(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();
}
